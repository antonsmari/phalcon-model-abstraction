<?php

/**
 * Copyright (c) 2013 Anton Smári Gunnarsson, Gabríel Arthúr Pétursson
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * ModelAbstraction provides automatic database function calling
 * and optionally caching function results
 *
 * Calls PostgreSQL functions automatically as PHP functions.
 *
 * @author    Anton Smári Gunnarsson, Gabríel Arthúr Pétursson
 * @copyright (c) 2013 Anton Smári Gunnarsson, Gabríel Arthúr Pétursson
 */

class ModelAbstraction extends \Phalcon\Mvc\Model
{
    private $object_count, $object_position, $objects;
    private static $memcache                    = NULL;
    private static $functions_initiated         = FALSE;
    private static $functions_prefix            = "appname_";
    private static $cache_prefix                = ""; //."_"
    private static $cache_default_time          = 900; //15 minutes (functions cache uses default)
    private static $cache_global_control_timer  = 0; //Don't touch
    private static $functions_sql               = "
		SELECT
			p.proname function_name,
			p.proretset function_returns_multiple,
			t.typname = 'record' function_returns_record
		FROM
			pg_catalog.pg_proc p
			JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid
			JOIN pg_catalog.pg_type t ON p.prorettype = t.oid
		WHERE
			n.nspname = :schema
	";
    private static $functions = array();

    private $abstraction_mode = Phalcon\Db::FETCH_OBJ;

    public function getSchema()
    {
        return "public";
    }

    public static function initiate_functions()
    {
        global $config;
        if (self::$functions_initiated) {
            return;
        }

        self::$functions_initiated = TRUE;

        if ($config->database->get('caching', false)) {
            $functions = unserialize(self::__memcache()->get(self::$cache_prefix.'available_functions'));

            if ($functions) {
                self::$functions = $functions;
                return;
            }
        }

        $model = new static();
        $functions = $model->getWriteConnection()->query(ModelAbstraction::$functions_sql, array('schema' => $model->getSchema()));
        $functions->setFetchMode(Phalcon\Db::FETCH_OBJ);

        while ($function = $functions->fetch()) {
            if (self::$functions_prefix === ""
                || strpos($function->function_name, self::$functions_prefix) === 0)
            {
                self::$functions[$function->function_name] = array(
                    'name'  => $function->function_name,
                    'multiple'  => $function->function_returns_multiple,
                    'record'  => $function->function_returns_record,
                );
            }
        }

        if ($config->database->get('caching', false)) {
            self::__memcache()->save(self::$cache_prefix.'available_functions', serialize(self::$functions));
        }
    }

    public function isValidDatabaseFunction($function_name)
    {
        if ( ! self::$functions_initiated) {
            self::initiate_functions();
        }

        if (isset(self::$functions[self::$functions_prefix.strtolower($function_name)]))
            return TRUE;

        return FALSE;
    }

    private function injectThis($arrayOrObject)
    {
        if (is_object($arrayOrObject)) {
            $obj_vars = get_object_vars($arrayOrObject);
            foreach ($obj_vars as $key => $value) {
                $this->{$key} = $value;
            }
            return $this;
        } else if (is_array($arrayOrObject)) {
            foreach ($arrayOrObject as $key => $value) {
                $this->{$key} = $value;
            }
            return $this;
        }

        return FALSE;
    }

    public static function cacheSet($name, $value, $lifetime, $name_optional = NULL)
    {
        self::__memcache()->save(self::$cache_prefix.'cc_'.$name.($name_optional !== NULL ? '_'.$name_optional : ''),
            $value, $lifetime);
    }

    public static function cacheGet($name, $name_optional = NULL)
    {
        $return =
            self::__memcache()->get(self::$cache_prefix.'cc_'.$name.($name_optional !== NULL ? '_'.$name_optional : ''));

        if ($return === NULL)
            return FALSE;

        $model = new static();
        if ( ! $model->injectThis($return)) {
            $model->{($name_optional != NULL ? $name_optional : $name)} = $return;
        }

        return $model;
    }

    /**
     * @usage: before calling a postgres function, will cache automatically based on name and parameters
     */
    public static function setCacheTime($lifetime)
    {
        if (is_integer($lifetime)) {
            self::$cache_global_control_timer = $lifetime;
        }
    }

    public static function clear_memcache()
    {
        $cached = self::__memcache()->queryKeys(self::$cache_prefix);
        foreach ($cached as $cache) {
            self::__memcache()->delete($cache);
        }
    }

    public static function debug_memcache()
    {
        $return = array();
        $cached = self::__memcache()->queryKeys(self::$cache_prefix);
        foreach ($cached as $cache) {
            $return[$cache] = self::__memcache()->get($cache);
        }
        return $return;
    }

    public static function __memcache()
    {
        if (self::$memcache === NULL) {
            $frontCache = new Phalcon\Cache\Frontend\Data(array(
                "lifetime" => self::$cache_default_time
            ));

            self::$memcache = new Phalcon\Cache\Backend\Memcache($frontCache, array(
                'host' => 'localhost',
                'port' => 11211,
                'persistent' => false
            ));
        }

        return self::$memcache;
    }

    private function __format_arguments_for_cache($arguments)
    {
        if (is_array($arguments) && array_diff_key($arguments, array_keys(array_keys($arguments)))) {
            return implode(';', array_keys($arguments))
            .'|'.implode(';', array_values($arguments));
        } else if (is_array($arguments)) {
            return implode(';', $arguments);
        } else if ( ! is_object($arguments)) {
            return $arguments;
        }
    }

    public function getRawResult()
    {
        if (isset($this->objects)) {
            return $this->objects;
        }

        return array();
    }

    public function previous()
    {
        if (isset($this->object_position) && isset($this->object_count) && $this->object_position - 1 >= 0 && $this->object_position < $this->object_count) {
            $this->injectThis($this->objects[--$this->object_position]);
            return $this;
        }

        return FALSE;
    }

    public function next()
    {
        if (isset($this->object_position) && isset($this->object_count) && $this->object_position >= 0 && $this->object_position + 1 < $this->object_count) {
            $this->injectThis($this->objects[++$this->object_position]);
            return $this;
        }

        return FALSE;
    }

    private function call($name, $arguments)
    {
        global $config;
        $function = self::$functions[$name];

        if ($function['multiple'] && $config->database->get('caching', false)
            && self::$cache_global_control_timer !== 0) {
            $cache_args = $this->__format_arguments_for_cache($arguments);

            $cached_result
                = unserialize(self::__memcache()->get(self::$cache_prefix.$name.'_'.$cache_args));

            if ($this->injectThis($cached_result)) {
                self::$cache_global_control_timer = 0;
                return $this;
            }
        }

        $parameters = array();
        $bindings = array();

        if (is_array($arguments)) {
            $is_assoc = (bool) array_diff_key($arguments, array_keys(array_keys($arguments)));

            foreach ($arguments as $key => $value)
            {
                if ( ! $is_assoc && is_int($key)) {
                    $parameters[] = ':'.$key;
                } else {
                    $parameters[] = $key.' := :'.$key;
                }

                if (is_bool($value)) {
                    $bindings[$key] = ($value ? 't' : 'f');
                } else {
                    $bindings[$key] = $value;
                }
            }
            $parameters = implode(', ', $parameters);
        } else {
            $parameters = ':arg';
            $bindings = array('arg' => $arguments);
        }

        $function_sql = 'SELECT * FROM '.$this->getSchema().'.'.$name.'('.$parameters.')';

        if ($function['multiple']) {
            $return = $this->getDI()->getDb()->fetchAll(
                $function_sql,
                $this->abstraction_mode,
                $bindings
            );

            if (is_array($return) && isset($return[0]) && (is_object($return[0]) || is_array($return[0])))
            {
                $this->object_position = 0;
                $this->object_count = count($return);
                $this->objects = $return;

                $this->injectThis($return[0]);
            }

            if ($config->database->get('caching', false)
                && $return && ! empty($return) && self::$cache_global_control_timer !== 0) {
                self::__memcache()->save(self::$cache_prefix.$name.'_'.$cache_args, serialize($return->dump()), self::$cache_global_control_timer);
            }
        } else {
            return $this->getDI()->getDb()->execute(
                $function_sql,
                $bindings
            );
        }

        self::$cache_global_control_timer = 0;

        return $this;
    }

    public function __call($name, $arguments)
    {
        if ( ! $this->isValidDatabaseFunction($name))
        {
            return parent::__call($name, $arguments);
        }

        if (is_array($arguments) && isset($arguments[0]) && count($arguments) == 1 && is_array($arguments[0])) {
            $arguments = $arguments[0];
        }

        return $this->call(self::$functions_prefix . strtolower($name), $arguments);
    }

    public static function __callStatic($name, $arguments)
    {
        $model = new static();
        if ( ! $model->isValidDatabaseFunction($name))
        {
            return parent::__callStatic($name, $arguments);
        }

        if (is_array($arguments) && isset($arguments[0]) && count($arguments) == 1 && is_array($arguments[0])) {
            $arguments = $arguments[0];
        }

        return $model->call(self::$functions_prefix . strtolower($name), $arguments);
    }

    public static function transaction($function)
    {
        $db = new static();

        $db->getWriteConnection()
            ->query('SET SESSION CHARACTERISTICS AS TRANSACTION ISOLATION LEVEL SERIALIZABLE')
            ->execute();
        $db->getWriteConnection()->query('BEGIN')->execute();

        while (true)
        {
            try
            {
                $success = $function($db);
                break;
            }
            catch (PDOException $e)
            {
                if ($e->getCode() == 40001) /* serialization failure */
                continue;

                throw $e;
            }
        }

        if ($success)
        {
            $db->getWriteConnection()->query('COMMIT')->execute();
        }
        else
        {
            $db->getWriteConnection()->query('ROLLBACK')->execute();
        }
    }

}
