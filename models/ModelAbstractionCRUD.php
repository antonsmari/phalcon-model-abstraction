<?php

/**
 * Copyright (c) 2013 Anton Smári Gunnarsson, Gabríel Arthúr Pétursson
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * ModelAbstractionCRUD extends the CRUD operations of Phalcon's built-in ORM with limited behavior.
 *
 * @author    Anton Smári Gunnarsson, Gabríel Arthúr Pétursson
 * @copyright (c) 2013 Anton Smári Gunnarsson, Gabríel Arthúr Pétursson
 */

class ModelAbstractionCRUD extends ModelAbstraction
{
    protected $_primary_key = 'id';
    public function save($data = NULL, $whiteList = NULL)
    {
        if ($this->abstract_crud) {
            if ($this->{$this->_primary_key} === NULL)
                return $this->create($data, $whiteList);
            else
                return $this->update($data, $whiteList);
        }

        return parent::save($data, $whiteList);
    }

    public function create($data = NULL, $whiteList = NULL)
    {
        $function = $this->getSource().'_insert';
        if ($this->abstract_crud) {
            if ($this->isValidDatabaseFunction($function)) {
                $modelArray = $this->dump();

                $newData = array();
                foreach ($modelArray as $key => $param) {
                    if ($key == $this->_primary_key && $param !== NULL)
                        $newData[$key] = $param;
                }

                return $this->$function($newData);
            }

            return FALSE;
        }

        return parent::create($data, $whiteList);
    }

    public function update($data = NULL, $whiteList = NULL)
    {
        $function = $this->getSource().'_update';
        if ($this->abstract_crud) {
            if ($this->isValidDatabaseFunction($function)) {
                return $this->$function($this->dump());
            }

            return FALSE;
        }

        return parent::update($data, $whiteList);
    }

    public function delete()
    {
        $function = $this->getSource().'_delete';
        if ($this->abstract_crud) {
            if ($this->isValidDatabaseFunction($function)
                && $this->{$this->_primary_key} !== NULL) {
                $this->$function($this->{$this->_primary_key});

                return new static();
            }

            return $this;
        }

        return parent::delete();
    }

    public static function fromID($identifier)
    {
        $model = new static();

        $function = $model->getSource().'_select';
        if ($model->abstract_crud && $model->isValidDatabaseFunction($function)) {
            $object = $model->$function($identifier);
            if (isset($model->{$model->_primary_key}) && $model->{$model->_primary_key} !== NULL) {
                return $model;
            }
        }

        return FALSE;
    }

}
