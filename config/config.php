<?php

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'    => 'Postgresql',
        'host'       => 'localhost',
        'username'   => 'postgres',
        'password'   => '',
        'dbname'     => 'databasename',
        'logging'    => 'profile',
        'caching'    => TRUE,
    ),
    'application' => array(
        'modelsDir'      => __DIR__ . '/../models/',
        'viewsDir'      => __DIR__ . '/../views/',
        'baseUri'        => '/',
    )
));
